package game;

import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameFrame extends Frame   {
	GameMapPanel mapPanel;
	Map map;
	Label label;
	Button button;
	

	GameFrame(Map k) {
		map = k;
		GameWindowListener gwl = new GameWindowListener();
		addWindowListener(gwl);
		
		mapPanel = new GameMapPanel(map);
		mapPanel.setBounds(30, 30, 301, 301);
		this.add(mapPanel);		

		
		button = new Button();
		button.setBounds(map.getSizex()*50 + 100, 80, 100, 50);
		button.setLabel("heal");
		this.add(button);
		
		GameButtonActionListener al = new GameButtonActionListener(this);
		button.addActionListener(al);
		
		label = new Label();
		label.setBounds(map.getSizex()*50 + 100, 130, 120, 50);
		label.setText("lose 30 gold to heal ");
		this.add(label);

	}
	

	
	

}
