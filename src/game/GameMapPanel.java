package game;

import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class GameMapPanel extends Panel implements Runnable{
	Map map;
	Thread thread;
	GameKeyListener gkl;
	GameMouseListener gml;
	int FPS =60;
	
	GameMapPanel(Map m) {
		map = m;
		gkl = new GameKeyListener(this);
		gml = new GameMouseListener(this);
		addKeyListener(gkl);
		addMouseListener(gml);
		
		
	}
	
	public void paint(Graphics g) {
		File f = new File("razboinik.png");
	   
	         
			try {
				BufferedImage img = ImageIO.read(f);
				 g.drawImage(img,50+map.razboinik.getPositionX()*50, 50+ map.razboinik.getPositionY()*50, 50, 50,null);
			} catch (IOException e) {
				
				e.printStackTrace();
			}
	       
	    
		int sizexInPixels = map.getSizex() * 50;
		int sizeyInPixels = map.getSizey() * 50;

		
		
		File f1 = new File("coin.png");
		try {
			BufferedImage img = ImageIO.read(f1);
			 g.drawImage(img,50 + map.getObjectX() * 50, 50 + map.getObjectY() * 50, 50, 50, null);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		
		
		
		for (int i = 0; i <= map.getSizex(); i++) {
			g.drawLine(50 + i * 50, 50, 50 + i * 50, 50 + sizeyInPixels);
		}
		for (int i = 0; i <= map.getSizey(); i++) {
			g.drawLine(50, 50 + i * 50, 50 + sizexInPixels, 50 + i * 50);
		}
	}
	
	public void startGameThread() {
		thread = new Thread(this);
		thread.start();
	}
	
	public void run() {

		double drawInterval = 1000000000/FPS;
		double delta =0;
		long lastTime = System.nanoTime();
		long currentTime;
		long timer =0;
		int drawCount =0;
		while(thread != null) {
//			
			currentTime =System.nanoTime();
			delta+= (currentTime - lastTime) / drawInterval;
			timer += (currentTime - lastTime);
			lastTime = currentTime;
					
			
			if(delta>=1) {
				update();
				repaint();
				delta--;
				drawCount++;
			}
			if(timer>= 1000000000) {
				
				drawCount=0;
				timer=0;
			}
		}
		
	}
	
	public void update() {
		if(gkl.getMovedUp()==true) {
			this.map.razboinik.setPositionY(this.map.razboinik.getPositionY() - 1);
			gkl.setMovedUp(false);
		}
		if(gkl.getMovedDown()==true) {
			this.map.razboinik.setPositionY(this.map.razboinik.getPositionY() + 1);
			gkl.setMovedDown(false);
		}
		if(gkl.getMovedLeft()==true) {
			this.map.razboinik.setPositionX(this.map.razboinik.getPositionX() - 1);
			gkl.setMovedLeft(false);
		}
		if(gkl.getMovedRight()==true) {
			this.map.razboinik.setPositionX(this.map.razboinik.getPositionX() + 1);
			gkl.setMovedRight(false);
		}
			
			
		this.map.interactWithObject();
		//this.repaint();
		if (this.map.razboinik.isAlive() == false) {
			System.out.println("Zagubi s  " + this.map.razboinik.getPari() + " zlato");
			System.exit(0);
		}
		this.requestFocus();
	}
	
	public void paintComponent(Graphics g) {	 
	 super.paintComponents(g);
	}

}
