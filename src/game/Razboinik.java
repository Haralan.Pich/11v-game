package game;

class Razboinik {
	private int pari;
	private int health = 100;
	private String name;
	private String orujie;
	private boolean imaOrujie = false;
	private int damage;
	private int positionX;
	private int positionY;
	public Map map;

	public int getPositionX() {
		return positionX;
	}

	public void setPositionX(int positionX) {
		if (positionX >= map.getSizex()) {
			positionX = map.getSizex();
		} else if (positionX < 0) {
			positionX = 0;
		} else {
			this.positionX = positionX;
		}
	}

	public int getPositionY() {
		return positionY;
	}

	public void setPositionY(int positionY) {
		if (positionY >= map.getSizey()) {
			positionY = map.getSizey();
		} else if (positionY < 0) {
			positionY = 0;
		} else {
			this.positionY = positionY;
		}
	}

	public void setPari(int p) {
		
		  if (p < 0) {
			this.pari = 0;
		} else {
			this.pari = p;
		}
	}

	public void setHealth(int h) {
		if (healthOver100(h) == true) {
			this.health = 100;
		}  if (h < 0) {
			this.health = 0;
		} else {
			this.health = h;
		}
	}

	public void setName(String n) {
		name = n;
	}

	public void setOrujie(String o) {
		orujie = o;
		imaOrujie = true;
		if (orujie == "mech") {
			setdamage(15);
		} else if (orujie == "bradva") {
			setdamage(20);
		} else if (orujie == "luk") {
			setdamage(10);
		}
	}

	public void setdamage(int d) {
		damage = d;
	}

	public int getDamage() {
		return damage;
	}

	public int getPari() {
		return pari;
	}

	public int getHealth() {
		return health;
	}

	public String getName() {
		return name;
	}

	public String getOrujie() {
		return orujie;
	}

	Razboinik(int p, String n, String o, int pX, int pY) {
		positionX = pX;
		positionY = pY;
		orujie = o;
		pari = p;
		name = n;
		setOrujie(o);

	}

	void kradeneOtNqkogo() {
		int min = 0, max = 100;
		pari = (int) Math.floor(Math.random() * (max - min + 1) + min);
		int oldHealth = health;
		health = (int) Math.floor(Math.random() * (max - min + 1) + min);
		setHealth(oldHealth - health);
		System.out.println(name + " specheli " + pari + " pari" + "\ni e na " + health + " hp");

	}

	void KrieSeVkushti() {
		health -= 15;
		pari -= 10;
		System.out.println("na " + name + " jivota  propadna " + pari + " pari mu ostavat i e na " + health + " jivot");
	}

	void jiveeZdravoslovno() {
		if(this.getPari()>=30) {
		setPari(this.getPari() - 30);
		setHealth(100);
		}
		else
		System.out.println("not enough money to heal");
		//System.out.println(name + " vuzvurna silite si no my ostanaha samo " + pari + " pari");
	}

	boolean isAlive() {
		if (health > 0) {
			return true;
		} else
			return false;
	}

	boolean healthOver100(int h) {

		if (h > 100) {
			return true;
		} else {
			return false;
		}
	}

	public static void main(String[] args) {

		Map map = new Map(5, 5);
		Razboinik Mitko = new Razboinik(50, "Mitko", "bradva", 2, 3);
		map.razboinik = Mitko;
		Mitko.map = map;
		map.createMap(5, 5);
		GameFrame f = new GameFrame(map);
		f.setLayout(null);
		f.setSize(f.getMaximumSize());
		f.setVisible(true);
		f.mapPanel.startGameThread();
		
		
		

	}
}
