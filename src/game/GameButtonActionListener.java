package game;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GameButtonActionListener implements ActionListener {
	GameFrame frame;
	
	GameButtonActionListener(GameFrame f) {
		frame = f;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		frame.map.razboinik.jiveeZdravoslovno();
		System.out.println("veche e na " + frame.map.razboinik.getHealth() + "hp " + " no e s " + frame.map.razboinik.getPari() + "pari");
	}
}
