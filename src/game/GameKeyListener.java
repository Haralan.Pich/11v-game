package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameKeyListener implements KeyListener {

	GameMapPanel panel;
	private Boolean movedUp=false;
	private Boolean movedDown=false;
	private Boolean movedLeft=false;
	private Boolean movedRight=false;
	
	GameKeyListener(GameMapPanel p) {
		panel = p;
	}
	


	@Override
	public void keyTyped(KeyEvent e) {
		char c = e.getKeyChar();
		switch (c) {
		case 'w':
			movedUp=true;
			
			break;
		case 's':
			movedDown=true;
			
			break;
		case 'a':
			movedLeft=true;
			
			break;
		case 'd':
			movedRight=true;
			
			break;
		}
		

	}

	

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		char c = e.getKeyChar();
		switch (c) {
		case 'w':
			movedUp=false;
			
			break;
		case 's':
			movedDown=false;
			
			break;
		case 'a':
			movedLeft=false;
			
			break;
		case 'd':
			movedRight=false;
			
			break;
		}

	}
	
	
	
	
	public Boolean getMovedUp() {
		return movedUp;
	}


	public void setMovedUp(Boolean movedUp) {
		this.movedUp = movedUp;
	}


	public Boolean getMovedDown() {
		return movedDown;
	}


	public void setMovedDown(Boolean movedDown) {
		this.movedDown = movedDown;
	}


	public Boolean getMovedLeft() {
		return movedLeft;
	}


	public void setMovedLeft(Boolean movedLeft) {
		this.movedLeft = movedLeft;
	}


	public Boolean getMovedRight() {
		return movedRight;
	}


	public void setMovedRight(Boolean movedRight) {
		this.movedRight = movedRight;
	}


}
